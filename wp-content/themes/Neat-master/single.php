<?php
/**
 * The template for displaying all single posts.
 *
 * @package Neat
 */

get_header(); ?>

	<?php
$context = Timber::get_context();
$context[ 'post' ] = new TimberPost(); 
Timber::render( 'welcome.twig', $context );
?>
	<!-- /.aa_single -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
