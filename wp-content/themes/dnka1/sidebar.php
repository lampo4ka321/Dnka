<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Dnka1
 */

// if ( ! is_active_sidebar( 'sidebar-1' ) ) {
// 	return;
// }
?>

<aside class="aside">

    <ul class="aside__block">
        <li class="aside__item">
            <a href="#" class="aside__item-link"><img src="http://dnka.lv/sites/dia/files/styles/partneris/public/media/partneri/f4b03fddd65c61731b36f613b45bb362.png" alt="G4S"></a>
        </li>
        <li class="aside__item">
            <a href="#"  class="aside__item-link"><img src="http://dnka.lv/sites/dia/files/styles/partneris/public/media/partneri/grifsag_logo_0.png" alt="GRIFS AG"></a>
        </li>
        <li class="aside__item">
            <a href="#"  class="aside__item-link"><img src="http://dnka.lv/sites/dia/files/styles/partneris/public/media/partneri/logo_securitas_1.jpg" alt="Securitas Latvia"></a>
        </li>
        <li class="aside__item">
            <a href="#"  class="aside__item-link"><img src="http://dnka.lv/sites/dia/files/styles/partneris/public/media/partneri/ldz_apsardze_logo_copy_3_0.png" alt="LDZ apsardze"></a>
        </li>
    </ul>
</aside>
