<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Dnka1
 */

?>

	</div><!-- #content -->

	<footer class="page-footer">
        <div class="page-footer__wrapper">
            <div class="page-footer__logo">
                <img src="wp-content/themes/dnka/assets/img/footerLogo.png" alt="footer_logo" class="page-footer__logo-img">
            </div>
            <h3 class="page-footer__title">DROŠĪBAS NOZARES KOMPĀNIJU ASOCIĀCIJA  | 2017</h3>
            <p>Visas tiesības paturētas</p>
        </div>
    </footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
