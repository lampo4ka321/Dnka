<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Dnka1
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" type="text/css" href="style-css-grid.css"/>
   


	<?php the_title( '<title>', '</title>' ); ?>

	<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

	<?php wp_head(); ?>
</head>

<body class="grid-container">
   
    <div class="header-line"></div>
    
    <header class="header">
        <div class="header__logo">
            <img src="wp-content/themes/dnka/assets/img/navBarLogo.png" alt="" class="header__logo-img">
        </div>

        <nav class="header__nav main-nav">
            <ul class="main-nav__block">
                <li class="main-nav__item">
                    <a href="/index.php?page_id=1765" class="main-nav__link">Par asocicāciju</a>
                    <ul class="main-nav__sub-block">
                        <li class="main-nav__sub-block-item">
                            <a href="#" class="main-nav__sub-block-item-link">Pārvalde</a>
                        </li>
                        <li class="main-nav__sub-block-item">
                            <a href="#" class="main-nav__sub-block-item-link">Kā kļūt par biedru</a>
                        </li>
                    </ul>
                </li>
                <li class="main-nav__item">
                    <a href="#" class="main-nav__link">Aktualitātes</a>
                </li>
                <li class="main-nav__item">
                    <a href="#" class="main-nav__link">Dokumenti</a>
                    <ul class="main-nav__sub-block">
                        <li class="main-nav__sub-block-item">
                            <a href="#" class="main-nav__sub-block-item-link">Memorands</a>
                        </li>
                        <li class="main-nav__sub-block-item">
                            <a href="#" class="main-nav__sub-block-item-link">Statūti</a>
                        </li>
                        <li class="main-nav__sub-block-item">
                            <a href="#" class="main-nav__sub-block-item-link">Nodomu protokols</a>
                        </li>
                        <li class="main-nav__sub-block-item">
                            <a href="#" class="main-nav__sub-block-item-link">Nozares pārskats 2017</a>
                        </li>
                    </ul>
                </li>
                <li class="main-nav__item">
                    <a href="#" class="main-nav__link">Biedri</a>
                    <ul class="main-nav__sub-block">
                        <li class="main-nav__sub-block-item">
                            <a href="#" class="main-nav__sub-block-item-link">G4S</a>
                        </li>
                        <li class="main-nav__sub-block-item">
                            <a href="#" class="main-nav__sub-block-item-link">GRIFS AG</a>
                        </li>
                        <li class="main-nav__sub-block-item">
                            <a href="#" class="main-nav__sub-block-item-link">Securitas Latvia</a>
                        </li>
                        <li class="main-nav__sub-block-item">
                            <a href="#" class="main-nav__sub-block-item-link">LDZ apsardze</a>
                        </li>
                    </ul>
                </li>
                <li class="main-nav__item">
                    <a href="#" class="main-nav__link">Kontakti</a>
                </li>
            </ul>
        </nav>
    </header>