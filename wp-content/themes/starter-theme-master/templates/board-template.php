<?php
/*
 * Template Name: Board
 * Template Post Type: page
 */


$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
Timber::render( 'board-template.twig', $context );