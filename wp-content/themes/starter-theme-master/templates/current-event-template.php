<?php
/*
 * Template Name: current-event-template
 * Template Post Type: page
 */


$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
Timber::render( 'current-event-template.twig', $context );