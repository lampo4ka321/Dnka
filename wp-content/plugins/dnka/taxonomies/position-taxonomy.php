<?php
if ( ! function_exists('rusanovs_register_position') ) {

// Register Custom Taxonomy
function rusanovs_register_position() {

	$labels = array(
		'name'                       => _x( 'Positions', 'Taxonomy General Name', 'rusanovs' ),
		'singular_name'              => _x( 'Position', 'Taxonomy Singular Name', 'rusanovs' ),
		'menu_name'                  => __( 'Position', 'rusanovs' ),
		'all_items'                  => __( 'All Items', 'rusanovs' ),
		'parent_item'                => __( 'Parent Item', 'rusanovs' ),
		'parent_item_colon'          => __( 'Parent Item:', 'rusanovs' ),
		'new_item_name'              => __( 'New Item Name', 'rusanovs' ),
		'add_new_item'               => __( 'Add New Item', 'rusanovs' ),
		'edit_item'                  => __( 'Edit Item', 'rusanovs' ),
		'update_item'                => __( 'Update Item', 'rusanovs' ),
		'view_item'                  => __( 'View Item', 'rusanovs' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'rusanovs' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'rusanovs' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'rusanovs' ),
		'popular_items'              => __( 'Popular Items', 'rusanovs' ),
		'search_items'               => __( 'Search Items', 'rusanovs' ),
		'not_found'                  => __( 'Not Found', 'rusanovs' ),
		'no_terms'                   => __( 'No items', 'rusanovs' ),
		'items_list'                 => __( 'Items list', 'rusanovs' ),
		'items_list_navigation'      => __( 'Items list navigation', 'rusanovs' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
		'show_in_rest'               => true
	);
	register_taxonomy( 'position', array( 'person' ), $args );

}
add_action( 'init', 'rusanovs_register_position', 0 );

}
