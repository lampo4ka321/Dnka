<?php

if ( ! function_exists('dnka_register_cpt_members') ) {

	// Register Custom Post Type
	function dnka_register_cpt_members() {

		$labels = array(
			'name'                  => _x( 'Members', 'Post Type General Name', 'dnka' ),
			'singular_name'         => _x( 'Member', 'Post Type Singular Name', 'dnka' ),
			'menu_name'             => __( 'Members', 'dnka' ),
			'name_admin_bar'        => __( 'Members', 'dnka' ),
			'archives'              => __( 'Item Archives', 'dnka' ),
			'attributes'            => __( 'Item Attributes', 'dnka' ),
			'parent_item_colon'     => __( 'Parent Item:', 'dnka' ),
			'all_items'             => __( 'All Items', 'dnka' ),
			'add_new_item'          => __( 'Add New Item', 'dnka' ),
			'add_new'               => __( 'Add New', 'dnka' ),
			'new_item'              => __( 'New Item', 'dnka' ),
			'edit_item'             => __( 'Edit Item', 'dnka' ),
			'update_item'           => __( 'Update Item', 'dnka' ),
			'view_item'             => __( 'View Item', 'dnka' ),
			'view_items'            => __( 'View Items', 'dnka' ),
			'search_items'          => __( 'Search Item', 'dnka' ),
			'not_found'             => __( 'Not found', 'dnka' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'dnka' ),
			'featured_image'        => __( 'Featured Image', 'dnka' ),
			'set_featured_image'    => __( 'Set featured image', 'dnka' ),
			'remove_featured_image' => __( 'Remove featured image', 'dnka' ),
			'use_featured_image'    => __( 'Use as featured image', 'dnka' ),
			'insert_into_item'      => __( 'Insert into item', 'dnka' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'dnka' ),
			'items_list'            => __( 'Items list', 'dnka' ),
			'items_list_navigation' => __( 'Items list navigation', 'dnka' ),
			'filter_items_list'     => __( 'Filter items list', 'dnka' ),
		);
		$args = array(
			'label'                 => __( 'Members', 'dnka' ),
			'description'           => __( 'Members about company', 'dnka' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'thumbnail', 'editor' ),
			// 'taxonomies'            => array( 'position' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-groups',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'post',
			'show_in_rest'          => true,
		);
		register_post_type( 'members', $args );
	}
	add_action( 'init', 'dnka_register_cpt_members', 0 );
}


?>