<?php

if ( ! function_exists('rusanovs_register_cpt_practice_area') ) {

	// Register Custom Post Type
	/**
	 *
	 */
	function rusanovs_register_cpt_practice_area() {

		$labels = array(
			'name'                  => _x( 'Practice areas', 'Post Type General Name', 'rusanovs' ),
			'singular_name'         => _x( 'Practice area', 'Post Type Singular Name', 'rusanovs' ),
			'menu_name'             => __( 'Practice areas', 'rusanovs' ),
			'name_admin_bar'        => __( 'Practice area', 'rusanovs' ),
			'archives'              => __( 'Item Archives', 'rusanovs' ),
			'attributes'            => __( 'Item Attributes', 'rusanovs' ),
			'parent_item_colon'     => __( 'Parent Item:', 'rusanovs' ),
			'all_items'             => __( 'All Items', 'rusanovs' ),
			'add_new_item'          => __( 'Add New Item', 'rusanovs' ),
			'add_new'               => __( 'Add New', 'rusanovs' ),
			'new_item'              => __( 'New Item', 'rusanovs' ),
			'edit_item'             => __( 'Edit Item', 'rusanovs' ),
			'update_item'           => __( 'Update Item', 'rusanovs' ),
			'view_item'             => __( 'View Item', 'rusanovs' ),
			'view_items'            => __( 'View Items', 'rusanovs' ),
			'search_items'          => __( 'Search Item', 'rusanovs' ),
			'not_found'             => __( 'Not found', 'rusanovs' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'rusanovs' ),
			'featured_image'        => __( 'Featured Image', 'rusanovs' ),
			'set_featured_image'    => __( 'Set featured image', 'rusanovs' ),
			'remove_featured_image' => __( 'Remove featured image', 'rusanovs' ),
			'use_featured_image'    => __( 'Use as featured image', 'rusanovs' ),
			'insert_into_item'      => __( 'Insert into item', 'rusanovs' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'rusanovs' ),
			'items_list'            => __( 'Items list', 'rusanovs' ),
			'items_list_navigation' => __( 'Items list navigation', 'rusanovs' ),
			'filter_items_list'     => __( 'Filter items list', 'rusanovs' ),
		);
		$args = array(
			'label'                 => __( 'Practice area', 'rusanovs' ),
			'description'           => __( 'Practice area list', 'rusanovs' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'thumbnail', ),
			'taxonomies'            => array( 'Practice area-category', 'audience', 'price-level', 'style' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-thumbs-up',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
			'show_in_rest'          => true,
		);
		register_post_type( 'practice-area', $args );
	}

	add_action( 'init', 'rusanovs_register_cpt_practice_area', 0 );
}

