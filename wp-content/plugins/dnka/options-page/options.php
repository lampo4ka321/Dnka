<?php
	function rusanovs_add_options_page() {
		if( function_exists('acf_add_options_page') ) {

			acf_add_options_page();
			acf_add_options_sub_page(__( 'Global', 'rusanovs' ));
//			acf_add_options_sub_page(__( 'Footer', 'rusanovs' ));
		}
	}

	add_action('init', 'rusanovs_add_options_page');
?>