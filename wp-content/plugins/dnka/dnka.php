<?php
/**
 * Plugin Name: Dnka features
 * Description: Create post types and taxonomy
 * Author: Vladislavs Jurčenko
 * Author URI: http://turn.lv
 * Version: 0.1.0
 * Text Domain: dnka
 *
 * 
 */


define('DNKA_PLUGIN_PATH', plugin_dir_path(__FILE__));

// Register post types
require_once DNKA_PLUGIN_PATH . 'post-types/members.php';


// Register taxonomies
// require_once DNKA_PLUGIN_PATH . 'taxonomies/position-taxonomy.php';


// Register options page
// require_once DNKA_PLUGIN_PATH . 'options-page/options.php';

// Register menus
// require_once DNKA_PLUGIN_PATH . 'menu-locations/menu-locations.php';

// Register widgets
//require_once DNKA_PLUGIN_PATH . 'widgets/custom-widgets.php';
